#include "cuda_runtime.h"
#include "device_launch_parameters.h"

#include <stdio.h>
#include <stdlib.h>
#include <string>
#include "Helper.h"

#define N 300
#define BLOCK_SIZE N*3 

bool* deviceCellak; 

__global__ void OneCycle(bool * elozoCellak)
{
	//__shared__ bool sharedElozoCellak[BLOCK_SIZE];
	int globalIndex = blockIdx.x * blockDim.x + threadIdx.x;

	if (globalIndex >= N*N)
	{
		return;
	}

	//sharedElozoCellak[threadIdx.x] = elozoCellak[globalIndex];
	__syncthreads();

	bool sajatAllapot = elozoCellak[globalIndex];
	//bool sajatAllapot = sharedElozoCellak[blockIdx.x];
    bool ujAllapot = false;
	int szomszedosSejtekSzama = 0;

	//if (globalIndex - N - 1 >= 0)	//bal-fels�
	//{
	//	if (threadIdx.x-N-1>=0)	//ha sharedben van
	//	{
	//		if (sharedElozoCellak[threadIdx.x - N - 1])
	//		{
	//			szomszedosSejtekSzama++;
	//		}
	//	}
	//	else if (elozoCellak[globalIndex - N - 1])
	//	{
	//		szomszedosSejtekSzama++;				
	//	}
	//}
	//if (globalIndex - N >= 0)	//fels�
	//{
	//	if (threadIdx.x - N >= 0)	//ha sharedben van
	//	{
	//		if (sharedElozoCellak[threadIdx.x - N])
	//		{
	//			szomszedosSejtekSzama++;
	//		}
	//	}
	//	else if (elozoCellak[globalIndex - N])
	//	{
	//		szomszedosSejtekSzama++;
	//	}
	//}
	//if (globalIndex - N + 1 >= 0)	//jobb-fels�
	//{
	//	if (threadIdx.x - N + 1 >= 0)	//ha sharedben van
	//	{
	//		if (sharedElozoCellak[threadIdx.x - N + 1])
	//		{
	//			szomszedosSejtekSzama++;
	//		}
	//	}
	//	else if (elozoCellak[globalIndex - N + 1])
	//	{
	//		szomszedosSejtekSzama++;
	//	}
	//}
	//if (globalIndex - 1 >= 0 && globalIndex % N !=0)	//bal (nem-e balsz�ls�)
	//{
	//	if (threadIdx.x - 1 >= 0)	//ha sharedben van
	//	{
	//		if (sharedElozoCellak[threadIdx.x - 1])
	//		{
	//			szomszedosSejtekSzama++;
	//		}
	//	}
	//	else if (elozoCellak[globalIndex - 1])
	//	{
	//		szomszedosSejtekSzama++;
	//	}
	//}
	//////--------saj�t maga
	//if (globalIndex + 1 < N * N && globalIndex+1 % N != 0)	//jobb (nem-e jobbsz�ls�)
	//{
	//	if (threadIdx.x + 1 < BLOCK_SIZE)	//ha sharedben van
	//	{
	//		if (sharedElozoCellak[threadIdx.x + 1])
	//		{
	//			szomszedosSejtekSzama++;
	//		}
	//	}
	//	else if (elozoCellak[globalIndex + 1])
	//	{
	//		szomszedosSejtekSzama++;
	//	}
	//}
	//if (globalIndex + N - 1 < N * N)	//bal-als�
	//{
	//	if (threadIdx.x + N - 1 < BLOCK_SIZE)	//ha sharedben van
	//	{
	//		if (sharedElozoCellak[threadIdx.x + N - 1])
	//		{
	//			szomszedosSejtekSzama++;
	//		}
	//	}
	//	else if (elozoCellak[globalIndex + N - 1])
	//	{
	//		szomszedosSejtekSzama++;
	//	}
	//}
	//if (globalIndex + N < N * N)	//als�
	//{
	//	if (threadIdx.x + N  < BLOCK_SIZE)	//ha sharedben van
	//	{
	//		if (sharedElozoCellak[threadIdx.x + N ])
	//		{
	//			szomszedosSejtekSzama++;
	//		}
	//	}
	//	else if (elozoCellak[globalIndex + N])
	//	{
	//		szomszedosSejtekSzama++;
	//	}
	//}
	//if (globalIndex + N + 1 < N * N)	//jobb-als�
	//{
	//	if (threadIdx.x + N + 1 < BLOCK_SIZE)	//ha sharedben van
	//	{
	//		if (sharedElozoCellak[threadIdx.x + N + 1])
	//		{
	//			szomszedosSejtekSzama++;
	//		}
	//	}
	//	if (elozoCellak[globalIndex + N + 1])
	//	{
	//		szomszedosSejtekSzama++;
	//	}
	//}

	//szomsz�d sz�ml�l�s
	if (globalIndex - N - 1 >= 0)	//bal-fels�
	{
		if (elozoCellak[globalIndex - N - 1])
		{
			szomszedosSejtekSzama++;				
		}
	}
	if (globalIndex - N >= 0)	//fels�
	{
		if (elozoCellak[globalIndex - N])
		{
			szomszedosSejtekSzama++;
		}
	}
	if (globalIndex - N + 1 >= 0)	//jobb-fels�
	{
		if (elozoCellak[globalIndex - N + 1])
		{
			szomszedosSejtekSzama++;
		}
	}
	if (globalIndex - 1 >= 0 && globalIndex % N !=0)	//bal (nem-e balsz�ls�)
	{
		if (elozoCellak[globalIndex - 1])
		{
			szomszedosSejtekSzama++;
		}
	}
	////--------saj�t maga
	if (globalIndex + 1 < N * N && globalIndex+1 % N != 0)	//jobb (nem-e jobbsz�ls�)
	{
		if (elozoCellak[globalIndex + 1])
		{
			szomszedosSejtekSzama++;
		}
	}
	if (globalIndex + N - 1 < N * N)	//bal-als�
	{
		if (elozoCellak[globalIndex + N - 1])
		{
			szomszedosSejtekSzama++;
		}
	}
	if (globalIndex + N < N * N)	//als�
	{
		if (elozoCellak[globalIndex + N])
		{
			szomszedosSejtekSzama++;
		}
	}
	if (globalIndex + N + 1 < N * N)	//jobb-als�
	{
		if (elozoCellak[globalIndex + N + 1])
		{
			szomszedosSejtekSzama++;
		}
	}

	if (sajatAllapot==true)//ha �l
	{
		if (szomszedosSejtekSzama==2 || szomszedosSejtekSzama==3)
		{
			ujAllapot = true;
		}
		else
		{
			ujAllapot = false;
		}
	}
	else //ha nem �l
	{
		if (szomszedosSejtekSzama == 3)
		{
			ujAllapot = true;
		}
	}
	__syncthreads();

	elozoCellak[globalIndex]=ujAllapot;
	
	__syncthreads();
}

void clearMem()
{
	cudaFree(deviceCellak);
}

void oneCycle(bool * output)
{
	int block_count = (N*N- 1) / BLOCK_SIZE + 1;
	OneCycle << <block_count, BLOCK_SIZE >> > (deviceCellak);
	cudaMemcpy(output, deviceCellak, N * N * sizeof(bool), cudaMemcpyDeviceToHost);
}

void setup()
{
	bool mezok[N * N];
	//felt�lt�s
	for (int i = 0; i < N * N; i++)
	{
		int esely = rand() % 100;
		if (esely<30)	//meh
		{
			mezok[i] = true;
		}
		else
		{
			mezok[i] = false;
		}
	}

	cudaMalloc((void**)&deviceCellak, N * N * sizeof(bool));
	cudaMemcpy(deviceCellak, mezok, N * N * sizeof(bool), cudaMemcpyHostToDevice);	//kezdeti cell�k
}
#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <iostream>
#include "Helper.h"
#include <vector>
#include <windows.h>

#define N 300
#define PIXEL_SIZE 3    
#define SCREEN_WIDTH N*PIXEL_SIZE+50
#define SCREEN_HEIGHT N*PIXEL_SIZE+50

GLFWwindow* window;

void rajzol(bool* cellak)
{
    std::vector<float> vertices =
    {
        // jobbra, fel
    };
    int sorFentrol = 0;
    int sorDarab = 0;
    int sejtDarab = 0;
    for (size_t i = 0; i < N * N; i++)
    {
        if (cellak[i] == true)
        {
            vertices.push_back(PIXEL_SIZE + PIXEL_SIZE * sorDarab);
            vertices.push_back(SCREEN_HEIGHT - PIXEL_SIZE * sorFentrol);
            vertices.push_back(0.0);

            vertices.push_back(0 + PIXEL_SIZE * sorDarab);
            vertices.push_back(SCREEN_HEIGHT - PIXEL_SIZE * sorFentrol);
            vertices.push_back(0.0);

            vertices.push_back(0 + PIXEL_SIZE * sorDarab);
            vertices.push_back(SCREEN_HEIGHT - PIXEL_SIZE - PIXEL_SIZE * sorFentrol);
            vertices.push_back(0.0);

            vertices.push_back(PIXEL_SIZE + PIXEL_SIZE * sorDarab);
            vertices.push_back(SCREEN_HEIGHT - PIXEL_SIZE - PIXEL_SIZE * sorFentrol);
            vertices.push_back(0.0);
            sejtDarab++;
        }
        sorDarab++;
        if (sorDarab == N)
        {
            sorDarab = 0;
            sorFentrol++;
        }
    }
    glClear(GL_COLOR_BUFFER_BIT);

    // Render OpenGL here
    glEnableClientState(GL_VERTEX_ARRAY); // tell OpenGL that you're using a vertex array for fixed-function attribute
    glVertexPointer(3, GL_FLOAT, 0, &vertices[0]); // point to the vertices to be used
    glDrawArrays(GL_QUADS, 0, 4 * sejtDarab); // draw the vertixes

    glDisableClientState(GL_VERTEX_ARRAY); // tell OpenGL that you're finished using the vertex arrayattribute

    // Swap front and back buffers
    glfwSwapBuffers(window);

    // Poll for and process events
    glfwPollEvents();
}

int rajzolSetup()
{
    // Initialize the library
    if (!glfwInit())
    {
        return -1;
    }

    // Create a windowed mode window and its OpenGL context
    window = glfwCreateWindow(SCREEN_WIDTH, SCREEN_HEIGHT, "Game of life", NULL, NULL);

    if (!window)
    {
        glfwTerminate();
        return -1;
    }

    // Make the window's context current
    glfwMakeContextCurrent(window);

    glViewport(0.0f, 0.0f, SCREEN_WIDTH, SCREEN_HEIGHT); // specifies the part of the window to which OpenGL will draw (in pixels), convert from normalised to pixels
    glMatrixMode(GL_PROJECTION); // projection matrix defines the properties of the camera that views the objects in the world coordinate frame. Here you typically set the zoom factor, aspect ratio and the near and far clipping planes
    glLoadIdentity(); // replace the current matrix with the identity matrix and starts us a fresh because matrix transforms such as glOrpho and glRotate cumulate, basically puts us at (0, 0, 0)
    glOrtho(0, SCREEN_WIDTH, 0, SCREEN_HEIGHT, 0, 1); // essentially set coordinate system
    glMatrixMode(GL_MODELVIEW); // (default matrix mode) modelview matrix defines how your objects are transformed (meaning translation, rotation and scaling) in your world
    glLoadIdentity(); // same as above comment
}

void rajzolBezar()
{
    glfwTerminate();
}

int main(void)
{
    bool output[N * N];
    setup();
    rajzolSetup();

    for (size_t i = 0; i < 300; i++)
    {
        oneCycle(output);
        rajzol(output);
        Sleep(100);
    }
    Sleep(2000);
    rajzolBezar();
    clearMem();
}